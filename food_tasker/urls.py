"""food_tasker URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views as auth_views
from django.conf import settings
from django.conf.urls.static import static
# from rest_framework_simplejwt.views import (
#     TokenObtainPairView,
#     TokenRefreshView,
# )

from tasker.views import RestaurantSignUpView, RestaurantUpdateView


urlpatterns = [
    path('admin/', admin.site.urls),
    path('restaurant/login/', auth_views.LoginView.as_view(template_name='registration/login.html'), name='login'),
    path('restaurant/logout/', auth_views.LogoutView.as_view(template_name="registration/logout.html"), name='logout'),
    path('restaurant/update/', RestaurantUpdateView.as_view(), name="restaurant-update"),
    path('restaurant/signup/', RestaurantSignUpView.as_view(), name='restaurant-signup'),
    path('', include('tasker.urls')),
    path('api/v1/social/', include('rest_framework_social_oauth2.urls')),
    path('api/v1/', include('tasker.routers')),
    # path('api/v1/login/', TokenObtainPairView.as_view(), name="token_obtain_pair"),
    # path('api/v1/refresh/', TokenRefreshView.as_view(), name="token_refresh"),
    path('api/v1/auth/', include('dj_rest_auth.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


admin.site.site_header = "Food Tasker"
admin.site.site_title = "Controls"
admin.site.index_title = "Admin Interface"