import uuid
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.auth import get_user_model

from phonenumber_field.modelfields import PhoneNumberField

class CustomUser(AbstractUser):
    pass

User = get_user_model()

class BaseAddressModel(models.Model):
    phone = PhoneNumberField(unique=True, null=False, blank=False)
    address = models.TextField()

    class Meta:
        abstract = True


class Restaurant(BaseAddressModel):
    owner = models.OneToOneField(to=User, related_name='restaurant', on_delete=models.CASCADE)
    name = models.CharField(max_length=254, verbose_name="Restaurant Name")
    logo = models.ImageField(upload_to="restaurant/logos", default="restaurant/logo.jpg")

    def __str__(self):
        return self.name


class Driver(BaseAddressModel):
    user = models.OneToOneField(to=User, on_delete=models.CASCADE, related_name="driver")
    avatar = models.CharField(max_length=250)
    location = models.CharField(max_length=500, blank=True)

    def __str__(self):
        return self.user.username


class Customer(BaseAddressModel):
    user = models.OneToOneField(to=User, on_delete=models.CASCADE, related_name="customer")
    avatar = models.CharField(max_length=250)

    def __str__(self):
        return self.user.get_full_name()



class Meal(models.Model):
    restaurant = models.ForeignKey(to=Restaurant, related_name="meal", on_delete=models.CASCADE)
    name = models.CharField(max_length=250)
    short_description = models.CharField(max_length=500)
    image = models.ImageField(upload_to="restaurant/meal/", blank=False)
    price = models.DecimalField(max_digits=8, decimal_places=2, default=0)

    def __str__(self):
        return self.name


class Order(models.Model):

    PLACED = 1
    ACCEPTED = 2
    READY = 3
    ONTHEWAY = 4
    DELIVERED = 5

    STATUS_CHOICES = (
        (PLACED, "Order Placed"),
        (ACCEPTED, "Accepted"),
        (READY, "Ready"),
        (ONTHEWAY, "On the way"),
        (DELIVERED, "Delivered"),
    )

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    customer = models.ForeignKey(Customer, on_delete=models.SET_NULL, related_name="order", null=True)
    restaurant = models.ForeignKey(Restaurant, on_delete=models.SET_NULL, related_name="order", null=True)
    driver = models.ForeignKey(Driver, on_delete=models.SET_NULL, null=True, blank=True)
    address = models.TextField()
    total = models.DecimalField(max_digits=8, decimal_places=2, default=0)
    status = models.IntegerField(choices=STATUS_CHOICES)
    created_at = models.DateTimeField(auto_now_add=True)
    picked_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        ordering = ordering = ['-created_at']

    def __str__(self):
        return f"{self.id} at {self.created_at}"


class ItemDetail(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name="item_details")
    meal = models.ForeignKey(Meal, on_delete=models.SET_NULL, null=True)
    quantity = models.PositiveIntegerField()
    sub_total = models.DecimalField(max_digits=8, decimal_places=2, default=0)

    def __str__(self):
        return str(self.id)



    