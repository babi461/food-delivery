from django.views.generic import View
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect, get_list_or_404, get_object_or_404

from .forms import SignUpFrom, RestaurantForm, CustomUserChangeForm, MealForm
from .models import Meal, Order

class RestaurantSignUpView(View):

    def get(self, request):
        user_form = SignUpFrom()
        restaurant_form = RestaurantForm()
        forms = {"user_form": user_form, "restaurant_form": restaurant_form}
        return render(request, template_name="registration/signup.html", context={"forms": forms})

    def post(self, request):
        user_form = SignUpFrom(request.POST)
        restaurant_form = RestaurantForm(request.POST, request.FILES)
        if user_form.is_valid() and restaurant_form.is_valid():
            user = user_form.save()
            new_restaurant = restaurant_form.save(commit=False)
            new_restaurant.owner = user
            new_restaurant.save()
            return redirect('login')
        forms = {"user_form": user_form, "restaurant_form": restaurant_form}
        return render(request, template_name="registration/signup.html", context={"forms": forms})


class HomePageView(LoginRequiredMixin, View):

    def get(self, request):
        return render(request, template_name='home.html', context={"data": "Welcome to Home Page!"})


class RestaurantUpdateView(LoginRequiredMixin, View):

    def get(self, request):
        user_form = CustomUserChangeForm(instance=request.user)
        restaurant_form = RestaurantForm(instance=request.user.restaurant)
        forms = {"user_form": user_form, "restaurant_form": restaurant_form}
        return render(request, template_name="registration/update_restaurant_info.html", context={"forms": forms})

    def post(self, request):
        user_form = CustomUserChangeForm(instance=request.user, data=request.POST)
        restaurant_form = RestaurantForm(instance=request.user.restaurant, data=request.POST, files=request.FILES)
        if user_form.is_valid() and restaurant_form.is_valid():
            user_form.save()
            restaurant_form.save()
            return redirect("home")
            forms = {"user_form": user_form, "restaurant_form": restaurant_form}
        return render(request, template_name="registration/update_restaurant_info.html", context={"forms": forms})

class MealListView(LoginRequiredMixin, View):

    def get(self, request):
        meals = get_list_or_404(Meal, restaurant=request.user.restaurant)
        return render(request, template_name="tasker/meal.html", context={"meals":meals})

class CreateMealView(LoginRequiredMixin, View):

    def get(self, request):
        form = MealForm()
        return render(request, template_name="tasker/add_meal.html", context={"form":form, "update": False})

    def post(self, request):
        form = MealForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            meal = form.save(commit=False)
            meal.restaurant = request.user.restaurant
            meal.save()
            return redirect("restaurant-meal")
        return render(request, template_name="tasker/add_meal.html", context={"form":form, "update": False})


class UpdateMealView(LoginRequiredMixin, View):

    def get(self, request, meal_id=None):
        meal = get_object_or_404(Meal, id=meal_id)
        form = MealForm(instance=meal)
        return render(request, template_name="tasker/add_meal.html", context={"form": form, "update": True})

    def post(self, request, meal_id=None):
        meal = get_object_or_404(Meal, id=meal_id)
        form = MealForm(instance=meal, data=request.POST, files=request.FILES)
        if form.is_valid():
            form.save()
            return redirect("restaurant-meal")
        return render(request, template_name="tasker/add_meal.html", context={"form": form, "update": True})


class RestaurantOrder(LoginRequiredMixin, View):

    def get(self, request):
        orders = get_list_or_404(Order, restaurant=request.user.restaurant)
        return render(request, template_name="tasker/order.html", context={"orders": orders})

    def post(get, request):
        order = get_object_or_404(Order, id=request.POST.get("id"), restaurant=request.user.restaurant)
        if order.status == Order.PLACED:
            order.status = Order.ACCEPTED
        elif order.status == Order.ACCEPTED:
            order.status = Order.READY
        order.save()
        return redirect("restaurant-order")


