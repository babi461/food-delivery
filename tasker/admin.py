from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth import get_user_model
from django.urls import resolve

from phonenumber_field.modelfields import PhoneNumberField
from phonenumber_field.widgets import PhoneNumberPrefixWidget

from .forms import SignUpFrom, CustomUserChangeForm
from .models import Restaurant, Meal, Driver, Customer, Order, ItemDetail


CustomUser = get_user_model()


class PhoneNumberWidget(admin.ModelAdmin):
    formfield_overrides = {
        PhoneNumberField : {'widget': PhoneNumberPrefixWidget},
    }


class ItemInline(admin.TabularInline):
    model = ItemDetail
    
    def get_parent_object_from_request(self, request):
        resolved = resolve(request.path_info)
        return self.parent_model.objects.get(pk=resolved.kwargs.get("object_id"))

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "meal":
            kwargs["queryset"] = Meal.objects.filter(restaurant=self.get_parent_object_from_request(request).restaurant)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)
    

@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):

    inlines = [
        ItemInline,
    ]

class MealInline(admin.TabularInline):
    model = Meal

@admin.register(Restaurant)
class RestaurantAdmin(PhoneNumberWidget):
    inlines = [
        MealInline,
    ]

admin.site.register(Driver, PhoneNumberWidget)
admin.site.register(Customer, PhoneNumberWidget)


# Remove below lines while deploying(below for debugging)

# admin.site.register(Meal)

# @admin.register(CustomUser)
# class CustomUserAdmin(UserAdmin):
#     add_form = SignUpFrom
#     form = CustomUserChangeForm
#     model = CustomUser
