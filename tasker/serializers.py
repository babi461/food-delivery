from rest_framework.serializers import ModelSerializer, SerializerMethodField, ReadOnlyField
from rest_framework.reverse import reverse

from .models import Restaurant, Meal, Customer, Driver, Order, ItemDetail


class MealSerializer(ModelSerializer):

    class Meta:
        model = Meal
        exclude = ("restaurant",)

class RestaurantSerializer(ModelSerializer):

    url = SerializerMethodField()
    menu = SerializerMethodField()

    class Meta:
        model = Restaurant
        exclude = ('id', 'owner')

    def get_url(self, restaurant):
        return reverse("restaurant-api-detail", kwargs={"res_id": restaurant.id}, request=self.context['request'])

    def get_menu(self, restaurant):
        return reverse("meal-api-list", kwargs={"res_id": restaurant.id}, request=self.context['request'])


class OrderCustomerSerializer(ModelSerializer):
    name = ReadOnlyField(source='user.get_full_name')

    class Meta:
        model = Customer
        exclude = ('id', 'user', 'avatar')


class OrderDriverSerializer(ModelSerializer):
    name = ReadOnlyField(source='user.get_full_name')

    class Meta:
        model = Driver
        exclude = ('id', 'user')


class OrderRestaurantSerializer(ModelSerializer):

    class Meta:
        model = Restaurant
        fields = ('name', 'phone', 'address',)


class OrderMealSerializer(ModelSerializer):

    class Meta:
        model = Meal
        fields = ('name', 'price')


class OrderItemDetailSerializer(ModelSerializer):
    meal = OrderMealSerializer(read_only=True)

    class Meta:
        model = ItemDetail
        fields = ('meal', 'quantity', 'sub_total')


class CustomerOrderSerializer(ModelSerializer):
    customer = OrderCustomerSerializer(read_only=True)
    restaurant = OrderRestaurantSerializer(read_only=True)
    driver = OrderDriverSerializer(read_only=True)
    item_details = OrderItemDetailSerializer(many=True, read_only=True)
    status = ReadOnlyField(source='get_status_display')

    class Meta:
        model = Order
        fields = "__all__"