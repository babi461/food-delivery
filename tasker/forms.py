from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth import get_user_model
from .models import Restaurant, Meal

from phonenumber_field.formfields import PhoneNumberField
from phonenumber_field.widgets import PhoneNumberPrefixWidget

class SignUpFrom(UserCreationForm):

    class Meta:
        model = get_user_model()
        fields = ("username", "email")


class CustomUserChangeForm(UserChangeForm):

    class Meta:
        model = get_user_model()
        fields = ("first_name", "last_name", "email")

class RestaurantForm(forms.ModelForm):

    phone = PhoneNumberField(region="IN", widget=PhoneNumberPrefixWidget)
    class Meta:
        model = Restaurant
        exclude = ("owner", )


class MealForm(forms.ModelForm):

    class Meta:
        model = Meal
        exclude = ("restaurant",)
