from django.urls import path

from .views import HomePageView, UpdateMealView, CreateMealView, MealListView, RestaurantOrder

urlpatterns = [
    path('', HomePageView.as_view(), name='home'),
    path('restaurant/meal/', MealListView.as_view(), name="restaurant-meal"),
    path('restaurant/meal/add/', CreateMealView.as_view(), name="restaurant-meal-add"),
    path('restaurant/meal/edit/<int:meal_id>', UpdateMealView.as_view(), name="restaurant-meal-edit"),
    path('restaurant/orders/', RestaurantOrder.as_view(), name="restaurant-order"),
]