from django.urls import path, re_path

from .apis import (RestaurantListAPI,
    RestaurantDetailAPI, MealListAPI, CustomerOrderAPI,
    get_order_notification, DriverOrderAPI,
    driver_pick_or_deliver_order, assign_order_to_driver)

urlpatterns = [

    # customer
    path('customer/restaurant/', RestaurantListAPI.as_view(), name="restaurant-api-list"),
    path('customer/restaurant/<int:res_id>/', RestaurantDetailAPI.as_view(), name="restaurant-api-detail"),
    path('customer/restaurant/<int:res_id>/meals/', MealListAPI.as_view(), name="meal-api-list"),
    path('customer/restaurant/order/latest/', CustomerOrderAPI.as_view(), name="order-api-latest"),
    path('customer/restaurant/order/create/', CustomerOrderAPI.as_view(), name="order-api-create"),

    # restaurant
    re_path('restaurant/order/notification/(?P<last_req_time>\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z)/$', get_order_notification, name="restaurant-order-notification-api"),

    #driver
    path('driver/orders/', DriverOrderAPI.as_view(), name="driver-api-orders"),
    path('driver/orders/<uuid:order>/assign/', assign_order_to_driver, name="driver-api-assign"),
    path('driver/orders/<uuid:order>/', driver_pick_or_deliver_order, name="driver-api-order-update"),
    
]