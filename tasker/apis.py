from datetime import datetime
from django.shortcuts import get_list_or_404, get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST, HTTP_201_CREATED

from django.contrib.auth import get_user_model

from .models import Restaurant, Meal, Customer, Order, ItemDetail
from .serializers import RestaurantSerializer, MealSerializer, CustomerOrderSerializer

class RestaurantListAPI(APIView):

    def get(self, request):
        restaurants = get_list_or_404(Restaurant)
        s_data = RestaurantSerializer(restaurants, many=True, context={"request": request})
        return Response(s_data.data, status=HTTP_200_OK)

class RestaurantDetailAPI(APIView):

    def get(self, request, res_id=None):
        restaurant = get_object_or_404(Restaurant, id=res_id)
        s_data = RestaurantSerializer(restaurant, context={'request': request})
        return Response(s_data.data, status=HTTP_200_OK)

class MealListAPI(APIView):

    def get(self, request, res_id=None):
        meal_list = get_list_or_404(Meal, restaurant__id=res_id)
        s_data = MealSerializer(meal_list, many=True, context={'request': request})
        return Response(s_data.data, status=HTTP_200_OK)


class CustomerOrderAPI(APIView):

    def get(self, request):
        order = Order.objects.filter(customer__user=request.user).first()
        if not order:
            return Response({"status": "failed", "error": "No orders found"})
        s_data = CustomerOrderSerializer(order, context={"request": request})
        return Response(s_data.data, status=HTTP_200_OK)

    def post(self, request):

        customer = get_object_or_404(Customer, user=request.user)

        if Order.objects.filter(customer=customer).exclude(status=Order.DELIVERED):
            return Response({"status": "failed", "error": "Cannot place another Order."},status=HTTP_400_BAD_REQUEST)
        
        if not request.data.get("address"):
            return Response({"status": "failed", "error": "Address is required"},status=HTTP_400_BAD_REQUEST)
        
        order_details = request.data.get("order_details")
        order_total = sum(get_object_or_404(Meal, id=meal.get("meal_id")).price * meal.get("quantity", 1) for meal in order_details)
        
        if len(order_details) > 0:
            order = Order.objects.create(
                customer=customer,
                restaurant_id=request.data.get("restaurant_id"),
                total=order_total,
                status=Order.PLACED,
                address=request.data.get("address")
            )

            for meal in order_details:
                ItemDetail.objects.create(
                    order=order,
                    meal_id=meal.get("meal_id"),
                    quantity=meal.get("quantity"),
                    sub_total=Meal.objects.get(id=meal["meal_id"]).price * meal["quantity"]
                )
            return Response({"status": "success", "order-ID":order.id}, status=HTTP_201_CREATED)
        return Response(data={"status": "failed", "error": "Invalid order details"},status=HTTP_400_BAD_REQUEST)

@api_view(['GET',])
def get_order_notification(request, last_req_time):
    # last_req_time = datetime.strptime(last_req_time)
    orders_count = Order.objects.filter(
        restaurant__owner=request.user,
        created_at__gt=last_req_time
    ).count()
    return Response(data={"count": orders_count}, status=HTTP_200_OK)


class DriverOrderAPI(APIView):

    def get(self, request):
        orders = Order.objects.filter(driver=request.user.driver, status__in=(Order.ONTHEWAY, Order.READY))
        if orders:
            s_data = CustomerOrderSerializer(orders, many=True)
            return Response(s_data.data, status=HTTP_200_OK)
        orders = Order.objects.filter(status=Order.READY).order_by("-status")
        s_data = CustomerOrderSerializer(orders, many=True)
        return Response(s_data.data, status=HTTP_200_OK)


@api_view(['GET',])
def driver_pick_or_deliver_order(request, order=None):
    order = get_object_or_404(Order, id=order, driver=request.user.driver)
    if order.status in (Order.READY, Order.ONTHEWAY):
        if order.status == Order.READY:
            order.status = Order.ONTHEWAY
            order.picked_at = datetime.now()
        elif order.status == Order.ONTHEWAY:
            order.status = Order.DELIVERED
        order.save()
        order.refresh_from_db()
        return Response(data={"status": "success", "order_status": order.get_status_display()}, status=HTTP_200_OK)
    return Response(data={"status": "failed", "error": "Cannot update on delivered order"}, status=HTTP_400_BAD_REQUEST)


@api_view(['GET',])
def assign_order_to_driver(request, order=None):
    order = get_object_or_404(Order, id=order)
    if order.status == Order.READY and not order.driver:
        order.driver = request.user.driver
        order.save()
        s_data = CustomerOrderSerializer(order)
        return Response(
            {
                "status" : "success",
                "order_deatils" : s_data.data
            },
            status=HTTP_200_OK
        )
    return Response({"status": "failed", "error": "cannot assign order"}, status=HTTP_400_BAD_REQUEST)





        
        